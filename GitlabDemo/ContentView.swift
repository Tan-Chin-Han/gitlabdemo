//
//  ContentView.swift
//  GitlabDemo
//
//  Created by Edimax on 2022/4/18.
//

import SwiftUI

struct ContentView: View {
    
    @State private var title = "Hello, world!"
    
    @State private var age = ""
    
    var body: some View {
        VStack {
            Text(title)
                .padding()
            TextField("age", text: $age)
                .padding()
                .accessibilityIdentifier("ageTextField")
            Button {
                title = "Hello, Jerry"
            } label: {
                Text("Save")
            }.accessibilityIdentifier("saveButton")

        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
