//
//  AppInfoService.swift
//  GitlabDemo
//
//  Created by Edimax on 2022/5/19.
//

import Foundation

class AppInfoService{

    var appInfo = AppInfo(name: "iOSProjSE" , version: "v1.0.1" , date: "2020-10-09" , author: "Enoxs" , remark: "iOS App : Tutor")

    func getAppInfo() -> AppInfo{
        return appInfo
    }
}

