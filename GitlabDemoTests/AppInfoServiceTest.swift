//
//  AppInfoServiceTest.swift
//  GitlabDemoUITests
//
//  Created by Edimax on 2022/5/19.
//

import Foundation
import XCTest
@testable import GitlabDemo

class AppInfoServiceTest: XCTestCase {
    var service = AppInfoService()
    
    func testGetAppInfo01() {
        let appInfo = service.getAppInfo()
        XCTAssertEqual("iOSProjSE", appInfo.name)
    }
    
    func testGetAppInfo02() {
        let appInfo = service.getAppInfo()
        XCTAssertEqual("v1.0.1", appInfo.version)
    }
}
